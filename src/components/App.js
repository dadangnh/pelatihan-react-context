import React, {createContext} from 'react';
import Other from './Other';

export const RootContext = createContext();

class App extends React.Component {
    state = {
        counter: 10
    }

    handleCounterPlus = () => {
        this.setState({
            counter: this.state.counter + 1
        });
    }

    handleCounterMinus = () => {
        this.setState({
            counter: this.state.counter - 1
        });
    }

    render() {
        return (
            <RootContext.Provider value={{
                state: this.state,
                handleCounterMinus: this.handleCounterMinus
            }}>
                <div>
                    App: {this.state.counter}
                    <button onClick={this.handleCounterPlus}>+</button>
                    <button onClick={this.handleCounterMinus}>-</button>
                    <Other />
                </div>
            </RootContext.Provider>
        )
    }
}

export default App;
