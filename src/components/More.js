import React, {useContext} from 'react';
import { RootContext } from './App';

const More = () => {
    const {state, handleCounterMinus} = useContext(RootContext);
    return (
        <div>
            More: {state.counter}
            <button onClick={() => handleCounterMinus()}>-</button>
        </div>
    );

}

export default More;
